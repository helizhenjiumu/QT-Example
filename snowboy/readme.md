1. 下载snowboy：
https://github.com/Kitt-AI/snowboy/archive/v1.3.0.tar.gz

2. 将snowboy-1.3.0.tar.gz解压到$HOME目录下。

3. 安装依赖软件包
```bash
sudo apt-get install libatlas-base-dev
```